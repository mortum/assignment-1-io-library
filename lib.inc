section .data

newline_char: db 10

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60 
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .next:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .next
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
	push rdi 
    call string_length
	pop rsi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
	mov rsi, 0xA
	jmp     print_char


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov  rax, 1           
    mov  rdi, 1           
    mov  rsi, rsp     
    mov  rdx, 1          
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    js .negative

    jmp print_uint

    .negative:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    ; т.к. все символы цифр закодированы по порядку от 0 до 9,
	; число можно делить на 10, прибавляя к полученному остатку код символа "0"
	; это и будет давать код нужного символа
	; далее символы кладутся в стёк, считается из количество
	; при переходе из цикла перевода числа в символы, начинается цикл печати
	; символы печатаются с помощью функции print_char
	mov rax, rdi
	mov r8, 10
	xor rcx, rcx
.dec_to_ASCII_loop:
	xor rdx, rdx
	div r8
	add rdx, "0"
	push rdx
	inc rcx
	cmp rax, 0
	jne .dec_to_ASCII_loop
.print_ASCII_uint_loop:
	pop rdi
	push rcx
	call print_char
	pop rcx
	dec rcx
	cmp rcx, 0
	jne .print_ASCII_uint_loop
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:
    xor rax, rax
    xor rcx, rcx
	mov rax, 1
.comp_sym:	
	mov r9b, byte[rdi+rcx]
	cmp r9b, byte [rsi + rcx]
	jnz .ret0
	cmp r9b,0
	jz .ret1
	inc rcx
	jmp .comp_sym
.ret1:
    ret
.ret0:
	xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor     rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall

    test rax, rax
    jle .EOF

    .success:
        pop rax
        ret

    .EOF: 
        pop rax
        xor rax, rax
        ret
 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r13
    push r14
    push r15

    mov r13, rdi
    mov r14, rsi
    mov r15, rcx
.read_sym:
    call read_char
    test rax, rax 
    jz .end
    cmp rax, ' ' ;space
    jz .stop_or_next
    cmp rax, `\t` ;tab
    jz .stop_or_next
    cmp rax, `\n`
    jz .stop_or_next
    mov [r13+r15], rax
    inc r15 
    cmp r15, r14
    jnl .buff_OF
    jmp .read_sym
.stop_or_next:
    cmp r15, 0 
    jz .read_sym ; если был прочитан 1 символ, что слово закончилось, иначе читаем след. символ
    jmp .end
.buff_OF:                                                                
    xor rax, rax ; возврат 0                                         
    xor rdx, rdx
    pop r15
    pop r14
    pop r13
    ret
.end:
    xor rax, rax
    mov [r13+r15], rax 
    mov rdx, r15 
    mov rax, r13 
    pop r15
    pop r14
    pop r13
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
    xor rdx, rdx
.read_symb:
    xor r9, r9
    mov r9b, byte[rdi]
    test r9, r9
    je .return
    cmp byte[rdi], '0'
    jl .return
    cmp byte[rdi], '9'
    jnle .return
    imul rax, 10
    sub r9, '0'
    add rax, r9 
    inc rdx
    inc rdi
    jmp .read_symb
.return:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax 
    xor rdx, rdx
    cmp byte [rdi], '-'
    jz .neg_int
    cmp byte [rdi], '+'                     
    jz .pos_int
    call parse_uint
    jmp .return
    .pos_int:
        inc rdi
        call parse_uint
        inc rdx
        jmp .return
    .neg_int:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
    .return:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    cmp rdx, 0
    je .buffer_overflow        
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    ja .buffer_overflow
    add rsi, rdx        
    add rdi, rdx
.copy:
    mov r9, rdi
    sub r9, rdx
    mov r8, [r9] 
    mov r9, rsi
    sub r9, rdx
    mov [r9], r8
    dec rdx
    test rdx, rdx
    jz .done
    jmp .copy

.buffer_overflow:
    xor rax, rax
.done:    
    ret
